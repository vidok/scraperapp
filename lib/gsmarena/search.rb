module Gsmarena
  class Search < Base
    def self.search q
      doc = get "results.php3?sQuickSearch=yes&sName=#{q}"
      doc.xpath('//*[@id="review-body"]/div/ul/li/a').map do |node|
        { href: node.attribute('href').value, name: node.text}
      end
    end
  end
end

module Gsmarena
  class Brand < Base

    def self.all
      doc = get '/makers.php3'
      doc.xpath('//*[@id="body"]/div/div[2]/table/tr/td/a').map do |row|
        name = row.children.first.text.capitalize
        url = row.attribute('href').value
        yield(name, url)
      end
    end

    def self.brand_devices brand_url
      doc = get brand_url
      urls = parse_pagination doc

      # parse first page
      devices = parse_device_list(doc)
      devices += urls.map do |url|
        doc = get url
        parse_device_list(doc)
      end.flatten

      devices.map {|d| yield(d[:name], d[:url])}
    end

    def self.parse_device_list doc
      doc.xpath('//*[@id="review-body"]/div/ul/li/a').map do |row|
        name = row.children.find{|c| c.name == 'strong'}.content
        url = row.attribute('href').value
        { name: name, url: url }
      end
    end

    def self.parse_pagination doc
      doc.xpath('//*[@id="body"]/div/div[3]/div[1]/a')
        .map{ |a| a.attribute('href').value }
    end
  end
end

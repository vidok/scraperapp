module Gsmarena
  class Base
    def self.connection
      @connection ||= Faraday.new( url: 'http://www.gsmarena.com/' ) do |faraday|
        faraday.request  :url_encoded
        faraday.response :logger
        faraday.adapter  Faraday.default_adapter
      end
    end

    def self.get url
      u = URI(url)
      if u.path.last(5) != '.php3' && u.path.last(4) != '.php'
        url = "#{u.path}.php?#{u.query}"
      end

      response = connection.get(url)
      Nokogiri::HTML(response.body)
    end
  end
end

module Gsmarena
  class Device < Base
    def self.device url
      doc = get url
      name = doc.xpath('//*[@id="body"]/div/div[1]/div/div[1]/h1').text
      return {} if name.empty?
      { name: name, url: url, additional_info: parse_additional_info(doc) }
    end

    def self.parse_additional_info content
      result = {}
      tables = content.xpath('//*[@id="specs-list"]/table')
      tables.each_with_index do |table|
        th = table.xpath('tr/th').first.text
        result[th] = {}
        table.xpath('tr').each do |row|
          tds = row.children.select{|node| node.name == 'td'}.map(&:text)
          result[th][tds.first] = tds.second
        end
      end
      result
    end
  end
end

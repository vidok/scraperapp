require 'rails_helper'

describe BrandsController do
  describe "#index" do
    it "returns all brends" do
      get :index
      body = JSON.parse(response.body)
      expect(response).to be_success
      expect(body).not_to be_empty
      expect(body.map(&:keys)).to all(be == ['name', 'url'])
    end
  end
end

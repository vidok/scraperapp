require 'rails_helper'

describe HomeController do
  describe "#index" do
    it "has success response" do
      get :index
      expect(response).to be_success
    end
  end

  describe "#search" do
    context "with exist brand" do
      it "returns any Apple devices" do
        get :search, params: { q: 'Apple' }
        body = JSON.parse(response.body)
        expect(response).to be_success
        expect(body).not_to be_empty
        expect(body.map{|r| r['name']}).to all(be =~ /apple/i)
        expect(body.map(&:keys)).to all(be == ['href', 'name'])
      end
    end

    context "with wrong brand" do
      it "returns empty result" do
        get :search, params: { q: 'WrongBrandWithWrongDevices' }
        body = JSON.parse(response.body)
        expect(response).to be_success
        expect(body).to be_empty
      end
    end
  end
end

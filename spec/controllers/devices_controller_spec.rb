require 'rails_helper'

describe DevicesController do
  describe "#index" do
    context "with existing brand" do
      it "has success response" do
        get :index, params: { brand_url: 'apple-phones-48' }
        body = JSON.parse(response.body)
        expect(response).to be_success
        expect(body).not_to be_empty
        expect(body.map(&:keys)).to all(be == ['name', 'url', 'additional_info'])
      end
    end

    context "with wrong brand" do
      it "has empty response" do
        get :index, params: { brand_url: 'wrongurltest' }
        body = JSON.parse(response.body)
        expect(response).to be_success
        expect(body).to be_empty
      end
    end
  end

  describe "#show" do
    context "with wrong device" do
      it "has empty response" do
        get :show, params: { brand_url: 'apple', url: 'wrongdevice' }
        body = JSON.parse(response.body)
        expect(response).to be_success
        expect(body).to be_empty
      end
    end

    context "with existing device" do
      it "has success response" do
        get :show, params: { brand_url: 'apple-phones-48', url: 'apple_iphone_se-7969' }
        body = JSON.parse(response.body)
        expect(response).to be_success
        expect(body).not_to be_empty
        expect(body.keys).to be == ['name', 'url', 'additional_info']
        expect(body['additional_info']).not_to be_empty
      end
    end
  end
end

$(document).ready(function() {

  /* load brands */
  var loadBrands = function() {
    $.ajax({
      url: '/brands',
      method: 'GET',
      dataType: 'json'
    }).success(function(data) {
      if (data.length == 0) {
        return onError('Brands not found');
      }

      updateSelectOptions('brands', data)
    }).error(onError)
  }

  var updateBrandsSelect = function(brands) {
    var options = ['<option>select brand</option>']
    brands.forEach(function(brand){
      var option = '<option value="' + brand.url + '">' + brand.name + '</option>'
      options.push(option)
    })

    $('select[name=brands]')
      .find('option')
      .remove()
      .end()
      .append(options)
  }

  /* loads brands when page will be load */
  loadBrands();

  var onError = function(error){
    alert(error);
  }

  $brands_select = $('select[name=brands]')
  $brands_select.change(function() {
    loadDevices($(this).val());
  })

  $('select[name=devices]').change(function() {
    loadDevice($brands_select.val(), $(this).val());
  })

  var loadDevices = function(brand_url) {
    $.ajax({
      url: '/brands/' + cleanUrl(brand_url) + '/devices',
      method: 'GET',
      dataType: 'json',
    }).success(function(data){
      if (data.length == 0) {
        return onError('Devices not found');
      }

      updateSelectOptions('devices', data)
    }).error(onError)
  }

  var updateSelectOptions = function(name, rows) {
    var options = ['<option>select ' + name + '</option>']
    rows.forEach(function(row){
      var option = '<option value="' + row.url + '">' + row.name + '</option>'
      options.push(option)
    })

    $('select[name=' + name + ']')
      .find('option')
      .remove()
      .end()
      .append(options)
  }

  var loadDevice = function(brand_url, device_url) {
    $.ajax({
      url: '/brands/' + cleanUrl(brand_url) + '/devices/' + cleanUrl(device_url),
      method: 'GET',
      dataType: 'json',
    }).success(function(data){
      $('#current_device').html('<pre>' + JSON.stringify(data, null, 4) + '</pre>')
    })
  }

  $search_q = $('input[name=q]')
  $('#search-form').submit(function() {
    search($search_q.val());
    return false;
  })

  var search = function(q) {
    $.ajax({
      url: '/search',
      method: 'GET',
      dataType: 'json',
      data: "q=" + q
    }).success(function(data){
      var html = "";
      data.forEach(function(row){
        html += "<div>" + row.name + "</div>"
      })
      $('#search-results').html(html)
    }).error(onError)
  }

  /* remove ".php" from url */
  var cleanUrl = function(url) {
    return url.split('.', 1)[0]
  }
})

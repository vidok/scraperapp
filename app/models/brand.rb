class Brand
  attr_reader :url, :name

  def initialize name, url
    @name = name
    @url = url
  end

  def self.all
    parser.all { |name, url| Brand.new(name, url) }
  end

  def self.devices url
    parser.brand_devices(url) { |name, url| Device.new(name, url)}
  end

  def self.parser
    Gsmarena::Brand
  end
end

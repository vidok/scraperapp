class Device
  attr_reader :url, :name, :additional_info

  def initialize name, url, additional_info=nil
    @name = name
    @url = url
    @additional_info = additional_info
  end

  def self.find url
    row = parser.device url
    return {} if row.empty?
    Device.new(row[:name], row[:url], row[:additional_info])
  end

  def self.parser
    Gsmarena::Device
  end
end

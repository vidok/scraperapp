class DevicesController < ApplicationController
  def index
    render json: Brand.devices(params[:brand_url])
  end

  def show
    render json: Device.find(params[:url])
  end
end

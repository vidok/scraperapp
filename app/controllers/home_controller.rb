class HomeController < ApplicationController
  def index
  end

  def search
    render json: Search.search(params[:q])
  end
end

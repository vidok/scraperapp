Rails.application.routes.draw do
  root to: "home#index"

  get "/models" => "home#models"
  get "/models/model" => "home#model"
  get "/search" => "home#search"

  resources :brands, only: [:index], param: :url do
    resources :devices, only: [:index, :show], param: :url
  end
end
